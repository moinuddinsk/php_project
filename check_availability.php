<?php

// include Function file
include_once('function.php');

class UsernameAvailability
{
    private $uusername;

    public function __construct()
    {
        // Object creation
        $this->uusername = new DB_con();
    }

    public function checkAvailability($username)
    {
        // Getting Post value
        $uname = $username;

        // Calling function
        $sql = $this->uusername->usernameavailblty($uname);
        $num = mysqli_num_rows($sql);

        if ($num > 0) {
            echo "<span style='color:red'> Username already associated with another account.</span>";
            echo "<script>$('#submit').prop('disabled',true);</script>";
        } else {
            echo "<span style='color:green'> Username available for Registration.</span>";
            echo "<script>$('#submit').prop('disabled',false);</script>";
        }
    }
}

// Create an instance of the UsernameAvailability class
$usernameAvailability = new UsernameAvailability();

// Call the checkAvailability method with the posted username value
if (isset($_POST["username"])) {
    $usernameAvailability->checkAvailability($_POST["username"]);
}

?>
