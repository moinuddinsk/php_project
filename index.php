<?php

// Include Function file
include_once('function.php');

class UserRegistration
{
    private $userdata;

    public function __construct()
    {
        // Object creation
        $this->userdata = new DB_con();
    }

    public function registerUser()
    {
        if (isset($_POST['submit'])) {
            // Posted Values
            $fname = $_POST['fullname'];
            $uname = $_POST['username'];
            $uemail = $_POST['email'];
            $pasword = md5($_POST['password']);

            // Function Calling
            $sql = $this->userdata->registration($fname, $uname, $uemail, $pasword);

            if ($sql) {
                // Message for successful insertion
                echo "<script>alert('Registration successful.');</script>";
                echo "<script>window.location.href='signin.php'</script>";
            } else {
                // Message for unsuccessful insertion
                echo "<script>alert('Something went wrong. Please try again');</script>";
                echo "<script>window.location.href='signin.php'</script>";
            }
        }
    }
}

// Create an instance of the UserRegistration class
$userRegistration = new UserRegistration();

// Call the registerUser method to handle form submission
$userRegistration->registerUser();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>User Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assests/style.css" rel="stylesheet">
    <script src="assests/jquery-1.11.1.min.js"></script>
    <script src="assests/bootstrap.min.js"></script>
    <script>
        function checkusername(va) {
            $.ajax({
                type: "POST",
                url: "check_availability.php",
                data: 'username=' + va,
                success: function (data) {
                    $("#usernameavailblty").html(data);
                }
            });
        }
    </script>
</head>

<body>
    <div class="center">
        <form class="form-horizontal" action='' method="POST">
            <fieldset>
                <div id="legend" class="text-center">
                    <legend class="">User Registration Page</legend>
                </div>

                <div class="control-group">
                    <!-- Fullname -->
                    <label class="control-label" for="username">Fullname</label>
                    <div class="controls">
                        <input type="text" id="username" name="fullname" placeholder="" class="input-xlarge" required="true">
                    </div>
                </div>

                <div class="control-group">
                    <!-- Username -->
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <input type="text" id="username" name="username" onblur="checkusername(this.value)" class="input-xlarge" required="true">
                        <span id="usernameavailblty"></span>
                    </div>
                </div>

                <div class="control-group">
                    <!-- E-mail -->
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input type="email" id="email" name="email" placeholder="" class="input-xlarge" required="true">
                    </div>
                </div>

                <div class="control-group">
                    <!-- Password-->
                    <label class="control-label" for="password">Password :</label>
                    <div class="controls ">
                        <input type="password" id="password" name="password" placeholder="" class="input-xlarge" required="true">
                    </div>
                </div>

                <div class="control-group ">
                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-success" type="submit" id="submit" name="submit">Register</button>
                    </div>
                </div>

                <div class="control-group ">
                    <div class="controls">
                        Already registered <a href="signin.php">Signin</a>
                    </div>
                </div>
            </fieldset>
        </form>
        <script type="text/javascript">
        </script>
    </div>
</body>

</html>
