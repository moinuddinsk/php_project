<?php

session_start();

class UserWelcome
{
    private $sessionUid;
    private $sessionFname;

    public function __construct()
    {
        // Check if user is not logged into the file
        if (strlen($_SESSION['uid']) == "") {
            header('location:logout.php');
        } else {
            // Initialize session variables
            $this->sessionUid = $_SESSION['uid'];
            $this->sessionFname = $_SESSION['fname'];
            $this->renderWelcomePage();
        }
    }

    private function renderWelcomePage()
    {
        ?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
            <title>User Registration Page</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="assests/style.css" rel="stylesheet">
            <script src="assests/jquery-1.11.1.min.js"></script>
            <script src="assests/bootstrap.min.js"></script>
        </head>

        <body>
            <form class="form-horizontal" action='' method="POST">
                <fieldset>
                    <div id="legend" class="text-center">
                        <legend class="">Welcome Back : <?php echo $this->sessionFname; ?></legend>
                    </div>

                    <div class="control-group text-center">
                        <!-- Button -->
                        <div class="controls">
                            <a href="logout.php" class="btn btn-success" type="submit" name="signin">Logout</a>
                        </div>
                    </div>
                </fieldset>
            </form>
            <script type="text/javascript">
            </script>
        </body>

        </html>
        <?php
    }
}

// Create an instance of the UserWelcome class
$userWelcome = new UserWelcome();

?>
