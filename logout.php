<?php

class SessionManager
{
    public function __construct()
    {
        session_start();
    }

    public function destroySession()
    {
        session_destroy();
        header('location: signin.php');
        exit(); // Ensure that script execution stops after redirect
    }
}

// Create an instance of the SessionManager class
$sessionManager = new SessionManager();

// Call the destroySession method to destroy the session
$sessionManager->destroySession();

?>
